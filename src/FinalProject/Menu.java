package FinalProject;

import java.sql.*;
import java.util.Scanner;

public class Menu {
    private final user User;
    public Menu(user user){
        this.User = user;
    }

    public void initialize() {

        final String dbURL = "jdbc:mysql://localhost:3306/trade";
        final String user = "root";
        final String password = "gfhjkm123";

        Scanner scanner = new Scanner(System.in);

        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {



            boolean quit = false;
            int choice;

            printInstructions();

            while (!quit) {
                System.out.println("Enter your choice: ");
                choice = scanner.nextInt();
                scanner.nextLine();

                switch (choice) {
                    case 0:
                        //print all options
                        printInstructions();
                        break;
                    case 1:
                        //To print all lines from the Trading Journal
                        readData(conn);
                        break;
                    case 2:
                        System.out.println("Enter date: ");
                        String date = scanner.nextLine();

                        System.out.println("Enter instrument: ");
                        String instrument = scanner.nextLine();

                        System.out.println("Enter ticker: ");
                        String ticker = scanner.nextLine();

                        System.out.println("Enter lots: ");
                        String lots = scanner.nextLine();

                        System.out.println("Long or Short: ");
                        String longShort = scanner.nextLine();

                        System.out.println("Enter entry price: ");
                        double entryPrice = scanner.nextDouble();

                        System.out.println("Enter exit price: ");
                        double exitPrice = scanner.nextDouble();

                        System.out.println("Profit or Loss: ");
                        double profitLoss = scanner.nextDouble();
                        scanner.nextLine();

                        System.out.println("Enter duration: ");
                        String duration = scanner.nextLine();

                        System.out.println("Enter strategy: ");
                        String strategy = scanner.nextLine();

                        insertData(conn, date, instrument, ticker, lots, longShort, entryPrice, exitPrice, profitLoss, duration, strategy);
                        break;
                    case 3:
                        //To remove a line from the Journal
                        System.out.println("Enter Trade ID you want to delete: ");
                        int tradeID = scanner.nextInt();

                        deleteData(conn, tradeID);
                        break;
                    case 4:
                        //To calculate the average profit or loss
                        averageProfitLoss(conn);
                        break;
                    case 5:
                        //To see min profit/loss
                        minProfitLoss(conn);
                        break;
                    case 6:
                        //To see max profit/loss
                        maxProfitLoss(conn);
                        break;
                    case 7:
                        //To calculate total profit or loss
                        totalProfitLoss(conn);
                        break;
                    case 8:
                        //One trader's profit
                        System.out.println("Enter Last name: ");
                        String profitLastName = scanner.nextLine();

                        oneTraderProfit(conn, profitLastName);
                        break;
                    case 9:
                        //To calculate Capital Gains Tax
                        System.out.println("Enter Last name: ");
                        String taxLastName = scanner.nextLine();

                        oneTraderTax(conn, taxLastName);
                        break;
                    case 10:
                        quit = true;
                        break;
                }
            }

        } catch (SQLException e) {
            System.out.println("Something went wrong");
        }
    }

    public static void printInstructions() {
        System.out.println("Press\n" +
                "0 - To print instructions\n" +
                "1 - To print all lines from the Trading Journal\n" +
                "2 - To insert a new line\n" +
                "3 - To remove a line from the Journal\n" +
                "4 - To calculate the average profit or loss for all traders\n" +
                "5 - To see min profit/loss \n" +
                "6 - To see max profit/loss \n" +
                "7 - To calculate total profit or loss \n" +
                "8 - To calculate one trader's profit \n" +
                "9 - To calculate Capital Gains Tax \n" +
                "10 - To quit the application");
    }

    public void readData(Connection conn) throws SQLException {
        String sql = "SELECT * FROM trade.Trading_journal as tj JOIN trade.users as u on tj.username = u.username;";

        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            int tradeID = resultSet.getInt(1);
            String date = resultSet.getString(2);
            String instrument = resultSet.getString(3);
            String ticker = resultSet.getString(4);
            int lots = resultSet.getInt(5);
            String longShort = resultSet.getString(6);
            double entryPrice = resultSet.getDouble(7);
            double exitPrice = resultSet.getDouble(8);
            double profitLoss = resultSet.getDouble(9);
            String duration = resultSet.getString(10);
            String strategy = resultSet.getString(11);
            String firstName = resultSet.getString(13);
            String lastName = resultSet.getString(14);


            String output = "Trade info: \n\t ID: %d \n\t First name: %s \n\t " +
                    "Last name: %s \n\t Date: %s \n\t Instrument: %s \n\t " +
                    "Ticker: %s \n\t Lots: %s \n\t Long or Short: %s \n\t " +
                    "Entry price: %f \n\t Exit price: %f \n\t Profit or Loss: %f \n\t " +
                    "Duration: %s \n\t Strategy: %s";

            System.out.printf((output) + "%n", tradeID, firstName, lastName, date, instrument, ticker, lots, longShort, entryPrice, exitPrice, profitLoss, duration, strategy);
        }
    }

    public void insertData(Connection conn, String date,
                           String instrument, String ticker, String lots, String longShort, double entryPrice,
                           double exitPrice, double profitLoss, String duration, String strategy) throws SQLException {

        String sql = "INSERT INTO Trading_journal (date,instrument,ticker,lots,longShort,entryPrice,exitPrice,profitLoss,duration,strategy,username) VALUES (?,?,?,?,?,?,?,?,?,?,?);";

        PreparedStatement preparedStatement = conn.prepareStatement(sql);

        preparedStatement.setString(1, date);
        preparedStatement.setString(2, instrument);
        preparedStatement.setString(3, ticker);
        preparedStatement.setString(4, lots);
        preparedStatement.setString(5, longShort);
        preparedStatement.setDouble(6, entryPrice);
        preparedStatement.setDouble(7, exitPrice);
        preparedStatement.setDouble(8, profitLoss);
        preparedStatement.setString(9, duration);
        preparedStatement.setString(10, strategy);
        preparedStatement.setString(11, User.username);

        int rowInserted = preparedStatement.executeUpdate();

        if (rowInserted > 0) {
            System.out.println("A new user was inserted successfully");
        } else {
            System.out.println("Something went wrong");
        }
    }

    public static void deleteData(Connection conn, int TradeID) throws SQLException {
        String sql = "DELETE FROM Trading_journal WHERE TradeID = ?";

        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, TradeID);

        int rowDeleted = preparedStatement.executeUpdate();

        if (rowDeleted > 0) {
            System.out.println("A user was deleted successfully");
        } else {
            System.out.println("Something went wrong");
        }
    }

    public static void averageProfitLoss(Connection conn) throws SQLException {
        String sql = "SELECT ROUND(AVG(ProfitLoss),2) FROM Trading_journal;";

        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            double average = resultSet.getDouble(1);
            System.out.println("The average profit or loss for all traders is: " + average);
        }
    }

    public static void minProfitLoss(Connection conn) throws SQLException {
        String sql = "SELECT MIN(ProfitLoss) FROM Trading_journal;";

        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            double min = resultSet.getDouble(1);
            System.out.println("The min profit or loss for all traders is: " + min);
        }
    }

    public static void maxProfitLoss(Connection conn) throws SQLException {
        String sql = "SELECT MAX(ProfitLoss) FROM Trading_journal;";

        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            double min = resultSet.getDouble(1);
            System.out.println("The max profit or loss for all traders is: " + min);
        }
    }



    public static void totalProfitLoss(Connection conn) throws SQLException {
        String sql = "SELECT SUM(ProfitLoss) FROM Trading_journal;";

        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            double total = resultSet.getDouble(1);
            System.out.println("Total profit or loss for all traders is: " + total);
        }
    }

    public static void oneTraderProfit(Connection conn, String lastName) throws SQLException {
        String sql = "SELECT SUM(ProfitLoss) FROM Trading_journal WHERE  LastName = ?";

        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, lastName);

        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            double profit = resultSet.getDouble(1);
            System.out.println("Total profit of trader " + lastName + " is " + profit);
        }
    }

    public static void oneTraderTax(Connection conn, String lastName) throws SQLException {
        String sql = "SELECT SUM(ProfitLoss) FROM Trading_journal WHERE LastName = ?";

        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, lastName);

        ResultSet resultSet = preparedStatement.executeQuery();


        while (resultSet.next()) {
            double profit = resultSet.getDouble(1);
            if (profit >= 0) {
                double tax = profit * 0.2;
                System.out.printf("Your Capital Gains Tax due is %.2f \n", tax);
            } else {
                System.out.println("You don't have to pay any tax due to capital loss");
            }
        }
    }
}
