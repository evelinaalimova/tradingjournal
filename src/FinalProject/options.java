package FinalProject;

;
import java.sql.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class options {


    final String dbURL = "jdbc:mysql://localhost:3306/trade";
    final String user = "root";
    final String password = "gfhjkm123";

    final user existingUser = new user();
    public FinalProject.user logIn() {
        String userName;
        String pswrd;
        boolean userExists = false;

        Scanner scanner = new Scanner(System.in);

        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {

            do {
                System.out.println("Please enter your username");
                userName = scanner.nextLine().trim();
                existingUser.setUserName(userName);
                System.out.println("Please enter your password");
                pswrd = scanner.nextLine().trim();
            } while (checkUserNameAndPass(conn, userName, pswrd)!= 1);
            //userMenu();
        } catch (SQLException e) {
            System.out.println("No connection " + e);
        }
        return null;
    }
    public static int checkUserNameAndPass (Connection conn, String username, String password) throws SQLException {

        String sql = "SELECT count(*) FROM users WHERE username = ? AND password = ?";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, username);
        preparedStatement.setString(2, password);
        ResultSet resultSet = preparedStatement.executeQuery();
        int count = 0;
        if (resultSet.next()) {
            count = resultSet.getInt(1);
            if (count == 1) {
                System.out.println("Connected");

            } else {
                System.out.println("Username or password is incorrect, please try again");
            }
        }
        return count;
    }
    public user userRegistration(){
        System.out.println("Registration...");
        Scanner scanner = new Scanner(System.in);
        try(Connection conn = DriverManager.getConnection(dbURL, user , password)){

            boolean matches = false;
            while (!matches){
                System.out.println("Please enter username");
                String username = scanner.nextLine();

                Pattern patternID = Pattern.compile("[a-zA-Z0-9]*");
                Matcher matcher = patternID.matcher(username);
                boolean validFormat = matcher.matches();
                if(validFormat){

                    String sql = "SELECT * FROM users WHERE username = ?";
                    PreparedStatement preparedStatement = conn.prepareStatement(sql);
                    preparedStatement.setString(1, username);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    // No rows returned
                    if(!resultSet.next()) {
                        System.out.println("Proceed with next step");
                        existingUser.username = username;
                        matches = true;
                    }
                }else{
                    System.out.println("Please try again, trader with this ID already exists");

                }


            }

            matches = false;
            while (!matches){
                System.out.println("Please enter your password");
                System.out.println("6 letters and 3 numbers");
                String pswrd = scanner.nextLine();
                Pattern patternPswrd = Pattern.compile("[a-zA-Z]{6}[0-9]{3}");
                Matcher matcher = patternPswrd.matcher(pswrd);
                matches = matcher.matches();
                if(matches){
                    System.out.println("Please proceed with next step");
                    existingUser.password = pswrd;
                }else{
                    System.out.println("Please try another password!");
                }
            }
            matches = false;
            while (!matches){
                System.out.println("Enter your name");
                String firstName = scanner.nextLine().toUpperCase().trim();
                Pattern patternName = Pattern.compile("[a-zA-Z]*");
                Matcher matcher = patternName.matcher(firstName);
                matches = matcher.matches();
                if (matches){
                    System.out.println("Name entered please continue");
                    existingUser.firstName = firstName;
                }else {
                    System.out.println("Please try once again");
                }
            }

            matches = false;
            while (!matches){
                System.out.println("Please enter your last name");
                String lastName = scanner.nextLine().toUpperCase().trim();
                Pattern patternLast = Pattern.compile("[a-zA-z]*");
                Matcher matcher = patternLast.matcher(lastName);
                matches = matcher.matches();
                if(matches){
                    System.out.println("Last name entered");
                    existingUser.lastName = lastName;
                }else{
                    System.out.println("Please try another last name");
                }
            }

            insertUser();
        } catch (SQLException e) {
            System.out.println("No connection " + e);
        }
        return existingUser;
    }

    private void insertUser () throws SQLException{
        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {
            String sql = "INSERT INTO users (username, password, name, lastname) Values (?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, existingUser.username);
            preparedStatement.setString(2, existingUser.password);
            preparedStatement.setString(3, existingUser.firstName);
            preparedStatement.setString(4, existingUser.lastName);
            int rowInserted = preparedStatement.executeUpdate();
            if (rowInserted > 0) {
                System.out.println("Your account was created successfully");

            } else {
                System.out.println("Something went wrong");
            }
        }

    }





}

