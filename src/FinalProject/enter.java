package FinalProject;

import java.util.Scanner;
public class enter {
    private static final options Options = new options();
    private static user User;

    static {
        User = new user();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to traders journal");
        System.out.println("Press\n" +
                "0 - To log in\n" +
                "1 - To register\n" +
                "Any key to quit\n");
        String choice = scanner.nextLine();
        switch (choice) {
            case "0":
                User = LogIn();
                break;
            case "1":
                User = Register();
                break;
            default:
                return;
        }

        Menu menu = new Menu(User);
        menu.initialize();

    }

    private static user LogIn(){
        return Options.logIn();
    }

    private static user Register(){
        return Options.userRegistration();
    }
}
